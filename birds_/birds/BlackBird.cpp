//
// Created by Denis on 19.09.2019.
//

#include "BlackBird.h"
#include <string>


std::string BlackBird::type() const {
    return "BlackBird";
}

std::string BlackBird::voice() const {
    return "karrr";
}
