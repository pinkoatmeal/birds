//
// Created by Denis on 19.09.2019.
//
#pragma once

#include <string>
#include "../SEX.cpp"


class Bird {
private:
    int age;
    std::string name;
    std::string surname;
    std::string fathername;
    SEX sex;
public:
    Bird();
    explicit Bird(std::string);
    explicit Bird(int);
    Bird(std::string, int);
    Bird(std::string, std::string, std::string, int, SEX);
    virtual std::string type() const = 0;
    virtual std::string voice() const = 0;
    int getAge() const;
    void setAge(int);
    std::string getName() const;
    void setName(std::string);
    std::string getSurname() const;
    void setSurname(std::string);
    std::string getFathername() const;
    void setFathername(std::string);
    SEX getSex() const;
    void setSex(SEX);
    std::string introduce() const;
    virtual Bird* reproduct(Bird*) = 0;
};
