//
// Created by Denis on 26.09.2019.
//
#include "Bird.h"
#include <string>
#include <utility>
#include "Owl.h"
#include <cstdlib>

Owl::Owl() {};

Owl::Owl(std::string name): Bird(name) {};

Owl::Owl(int age): Bird(age) {};

std::string Owl::type() const {
    return "Owl";
}

std::string Owl::voice() const {
    return "Urur";
}

Owl::Owl(string firstname, string surname, string fathername, int age, SEX sex) :
Bird(firstname, surname, fathername, age, sex) {}

Bird* Owl::reproduct(Bird* partner) {
    string firstname = this->getName() + partner->getName();
    string surname;
    string fathername;
    SEX sex;
    if (partner->getSex() == MALE){
        surname = this->getSurname();
        fathername = partner->getFathername();
    }
    else
    {
        surname = partner->getSurname();
        fathername = this->getFathername();
    }

    int _rand = (rand() % 2);
    if (_rand == 0)
    {
        sex = MALE;
    }
    else
    {
        sex = FEMALE;
    }
    return new Owl(firstname, surname, fathername, 0, sex);
}




