//
// Created by Denis on 19.09.2019.
//

#include "Sparrow.h"
#include <string>

Sparrow::Sparrow(std::string name, int age): Bird(name, age) {}

std::string Sparrow::voice() const {
    return "bottle of rum";
}

std::string Sparrow::type() const {
    return "Sparrow";
}

Sparrow::Sparrow(string firstname, string surname, string fathername, int age, SEX sex) : Bird(firstname, surname,
                                                                                               fathername, age, sex) {

}

Bird* Sparrow::reproduct(Bird* partner) {
    string firstname = this->getName() + partner->getName();
    string surname;
    string fathername;
    SEX sex;
    if (partner->getSex() == MALE){
        surname = this->getSurname();
        fathername = partner->getFathername();
    }
    else
    {
        surname = partner->getSurname();
        fathername = this->getFathername();
    }

    int _rand = (rand() % 2);
    if (_rand == 0)
    {
        sex = MALE;
    }
    else
    {
        sex = FEMALE;
    }
    return new Sparrow(firstname, surname, fathername, 0, sex);
}


