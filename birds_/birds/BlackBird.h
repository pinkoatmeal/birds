//
// Created by Denis on 19.09.2019.
//
#pragma once

#include "Bird.h"
#include <string>


class BlackBird: public Bird {
public:
    [[nodiscard]]
    std::string type() const override;

    [[nodiscard]]
    std::string voice() const override;
};
