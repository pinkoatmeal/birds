//
// Created by Denis on 19.09.2019.
//

#include "Dove.h"
#include <string>
#include <cstdlib>

using namespace std;

std::string Dove::type() const {
    return "Dove";
}

std::string Dove::voice() const {
    return "kurlyk";
}

Dove::Dove(string firstname, string surname, string fathername, int age, SEX sex) :
Bird(firstname, surname, fathername, age, sex) {}

Bird* Dove::reproduct(Bird* partner) {
    string firstname = this->getName() + partner->getName();
    string surname;
    string fathername;
    SEX sex;
    if (partner->getSex() == MALE)
    {
        surname = this->getSurname();
        fathername = partner->getFathername();
    }
    else
    {
        surname = partner->getSurname();
        fathername = this->getFathername();
    }

    int _rand = (rand() % 2);
    if (_rand == 0)
    {
        sex = MALE;
    }
    else
    {
        sex = FEMALE;
    }
    return new Dove(firstname, surname, fathername, 0, sex);
}



