//
// Created by Denis on 19.09.2019.
//
#pragma once

#include "Bird.h"
#include <string>

using namespace std;

class Dove: public Bird {
public:

    Dove(string firstname, string surname, string fathername, int age, SEX sex);

    [[nodiscard]]
    std::string type() const override;

    [[nodiscard]]
    std::string voice() const override;

    Bird * reproduct(Bird*) override;

};