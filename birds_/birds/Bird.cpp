//
// Created by Denis on 19.09.2019.
//

#include "Bird.h"
#include <string>
#include <utility>

Bird::Bird() {}

Bird::Bird(std::string name): name(name) {}

Bird::Bird(int age): age(age) {}

Bird::Bird(std::string name, int age): name(name), age(age) {}

int Bird::getAge() const {
    return this->age;
}

void Bird::setAge(int age) {
    this->age = age;
}

std::string Bird::getName() const {
    return name;
}

void Bird::setName(std::string name) {
    this->name = std::move(name);
}

std::string Bird::getFathername() const {
    return fathername;
}

void Bird::setFathername(std::string fathername) {
    this->fathername = fathername;
}

std::string Bird::getSurname() const {
    return surname;
}

void Bird::setSurname(std::string surname) {
    this->surname = surname;
}

void Bird::setSex(SEX sex) {
    this->sex = sex;
}

SEX Bird::getSex() const {
    return sex;
}

 std::string Bird::introduce() const {
     return std::string("Hello, I'm " + type() + ". My name is " + getSurname() + " "
     + getName() + " " + getFathername() + ". I'm " + std::to_string(getAge()) + " years old. " + voice());
 }

Bird::Bird(std::string firstname, std::string surname, std::string fathername, int age, SEX sex):
name(firstname), surname(surname), fathername(fathername), age(age), sex(sex) {}