//
// Created by Denis on 26.09.2019.
//
#pragma once

#include "Bird.h"
#include <string>
using namespace std;
class Owl: public Bird {
public:
    explicit Owl(std::string);
    explicit Owl(int);
    explicit Owl();
    Owl(string firstname, string surname, string fathername, int age, SEX sex);

    Bird * reproduct(Bird*) override;

    std::string type() const;

    std::string voice() const;
};