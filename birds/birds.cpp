﻿#define GARBAGE false

#include <iostream>
#include <vector>

#include "../birds_/birds/Dove.h"
#include "../birds_/birds/Bird.h"
#include "../birds_/birds/BlackBird.h"
#include "../birds_/birds/Sparrow.h"
#include "../birds_/birds/Owl.h"
#include "../birds_/SEX.cpp"

using namespace std;

const int YEARS = 2;

int main()
{

	string _type;
	string _name;
	string _surname;
	string _fathername;
	SEX _sex;
	string tmp;
	int _age;

	vector<Bird*> _bird_house;

	do {
		cout << "Type: ";
		cin >> _type;
		if (_type == "No" || _type == "no")
		{
			break;
		}
		cout << "Name: ";
		cin >> _name;
		cout << "Suraname: ";
		cin >> _surname;
		cout << "Fathername: ";
		cin >> _fathername;
		cout << "Age: ";
		cin >> _age;
		cout << "Sex: ";
		cin >> tmp;

		if(tmp =="male")
			_sex = MALE;
		else
			_sex = FEMALE;

		if (_type == "Dove" || _type == "dove")
			_bird_house.push_back(new Dove(_name, _surname, _fathername, _age, _sex));
		if (_type == "Owl" || _type == "owl")
			_bird_house.push_back(new Owl(_name, _surname, _fathername, _age, _sex));
		if (_type == "Sparrow" || _type == "sparrow")
			_bird_house.push_back(new Sparrow(_name, _surname, _fathername, _age, _sex));

	} while (true);


	// _bird_house.push_back(new Owl("kek", "lol", "arbidol", 10, MALE));
	// _bird_house.push_back(new Owl("sho", "hmm", "aaa", 3, FEMALE));
	// _bird_house.push_back(new Owl("jack", "nonsparrow", "davich", 4, MALE));
	// _bird_house.push_back(new Dove("kesha", "nekesha", "nekeshovich", 9, MALE));
	// _bird_house.push_back(new Dove("sharik", "nesharik", "nesharikovich", 7, FEMALE));
	// _bird_house.push_back(new Sparrow("fedor", "nefedor", "nefedorovich", 2, MALE));


	for (Bird* _bird : _bird_house)
		cout << _bird->introduce() << endl;

	for (int year = 0; year < YEARS; year++)
	{
		int houseSize = _bird_house.size();
		vector<bool> isPregant(houseSize, false);
		
		for (int i = 0; i < houseSize; i++)
		{
			_bird_house[i]->setAge(_bird_house[i]->getAge() + 1);
			if (isPregant[i])
			{
				continue;
			}
			for (int j = i; j < houseSize; j++)
			{
				if ((_bird_house[i]->getSex() != _bird_house[j]->getSex()) && (_bird_house[i]->type() == _bird_house[j]->type()) && !isPregant[j])
				{
					_bird_house.push_back(_bird_house[i]->reproduct(_bird_house[j]));
					isPregant[i] = true;
					isPregant[j] = true;
					break;
				}
			}
		}

	}

	cout << "==============================================\n";
	for (Bird* _bird : _bird_house)
		cout << _bird->introduce() << endl;

	for (Bird* obj : _bird_house)
		delete obj;

	return 0;

}